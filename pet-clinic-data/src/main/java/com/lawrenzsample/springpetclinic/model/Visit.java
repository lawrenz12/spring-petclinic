package com.lawrenzsample.springpetclinic.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "tblVisits")
public class Visit extends BaseEntity {

    @Column(name = "visitDate")
    private LocalDate visitDate;

    @Column(name = "visitDescription")
    private String visitDescription;

    @ManyToOne
    @JoinColumn(name = "pet_id")//name - kung anong column ang foreign key
    private Pet pet;

    public LocalDate getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(LocalDate visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitDescription() {
        return visitDescription;
    }

    public void setVisitDescription(String visitDescription) {
        this.visitDescription = visitDescription;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
}
