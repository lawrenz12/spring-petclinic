package com.lawrenzsample.springpetclinic.repositories;

import com.lawrenzsample.springpetclinic.model.Owner;
import org.springframework.data.repository.CrudRepository;

public interface OwnerRepository extends CrudRepository<Owner, Long> {
    //repositories para sa lahat ng entities

    Owner findByLastName(String lastName);
}
