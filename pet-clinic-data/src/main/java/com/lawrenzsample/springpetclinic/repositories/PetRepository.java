package com.lawrenzsample.springpetclinic.repositories;

import com.lawrenzsample.springpetclinic.model.Pet;
import org.springframework.data.repository.CrudRepository;

public interface PetRepository extends CrudRepository<Pet, Long> {
}
