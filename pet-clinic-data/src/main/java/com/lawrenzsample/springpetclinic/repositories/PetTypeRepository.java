package com.lawrenzsample.springpetclinic.repositories;

import com.lawrenzsample.springpetclinic.model.PetType;
import org.springframework.data.repository.CrudRepository;

public interface PetTypeRepository extends CrudRepository<PetType, Long> {
}
