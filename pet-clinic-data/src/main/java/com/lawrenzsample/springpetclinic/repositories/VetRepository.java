package com.lawrenzsample.springpetclinic.repositories;

import com.lawrenzsample.springpetclinic.model.Vet;
import org.springframework.data.repository.CrudRepository;

public interface VetRepository extends CrudRepository<Vet, Long> {
}
