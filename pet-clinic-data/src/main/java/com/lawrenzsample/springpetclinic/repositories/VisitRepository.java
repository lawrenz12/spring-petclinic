package com.lawrenzsample.springpetclinic.repositories;

import com.lawrenzsample.springpetclinic.model.Visit;
import org.springframework.data.repository.CrudRepository;

public interface VisitRepository extends CrudRepository<Visit, Long> {
}
