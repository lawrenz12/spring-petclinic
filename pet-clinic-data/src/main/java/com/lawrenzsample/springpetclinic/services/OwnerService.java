package com.lawrenzsample.springpetclinic.services;

import com.lawrenzsample.springpetclinic.model.Owner;

import java.util.Set;

public interface OwnerService extends CrudService<Owner, Long>{

    Owner findBylastName(String lastName);
}
