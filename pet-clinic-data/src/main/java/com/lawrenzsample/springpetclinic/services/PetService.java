package com.lawrenzsample.springpetclinic.services;

import com.lawrenzsample.springpetclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long>{

}
