package com.lawrenzsample.springpetclinic.services;

import com.lawrenzsample.springpetclinic.model.PetType;


public interface PetTypeService extends CrudService<PetType, Long>{
}
