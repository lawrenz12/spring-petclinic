package com.lawrenzsample.springpetclinic.services;

import com.lawrenzsample.springpetclinic.model.Specialty;

public interface SpecialtyService extends CrudService<Specialty, Long>{
}
