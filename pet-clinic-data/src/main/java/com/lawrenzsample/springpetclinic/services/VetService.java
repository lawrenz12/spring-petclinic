package com.lawrenzsample.springpetclinic.services;

import com.lawrenzsample.springpetclinic.model.Vet;

import java.util.Set;

public interface VetService extends CrudService<Vet, Long>{
}
