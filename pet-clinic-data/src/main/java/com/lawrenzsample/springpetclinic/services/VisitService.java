package com.lawrenzsample.springpetclinic.services;

import com.lawrenzsample.springpetclinic.model.Visit;

import java.util.Set;

public interface VisitService extends CrudService<Visit, Long> {
}
