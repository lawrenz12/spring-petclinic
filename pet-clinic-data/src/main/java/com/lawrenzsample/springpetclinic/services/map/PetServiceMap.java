package com.lawrenzsample.springpetclinic.services.map;

import com.lawrenzsample.springpetclinic.model.Pet;
import com.lawrenzsample.springpetclinic.services.CrudService;
import com.lawrenzsample.springpetclinic.services.PetService;
import com.sun.xml.internal.bind.v2.model.core.ID;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Profile({"default", "map"})
public class PetServiceMap extends AbstractMapService<Pet, Long> implements PetService {

    @Override
    public Set<Pet> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(Pet object) {
        super.delete(object);
    }

    @Override
    public Pet save(Pet object) {
        return super.save(object);
    }

    @Override
    public void deleteByID(Long id) {
        super.deleteById(id);
    }

    @Override
    public Pet findById(Long id) {
        return super.findById(id);
    }
}
