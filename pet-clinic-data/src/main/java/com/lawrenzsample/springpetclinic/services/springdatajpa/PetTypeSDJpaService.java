package com.lawrenzsample.springpetclinic.services.springdatajpa;

import com.lawrenzsample.springpetclinic.model.PetType;
import com.lawrenzsample.springpetclinic.repositories.PetTypeRepository;
import com.lawrenzsample.springpetclinic.services.PetTypeService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class PetTypeSDJpaService implements PetTypeService {

    private final PetTypeRepository petTypeRepository;

    public PetTypeSDJpaService(PetTypeRepository petTypeRepository) {
        this.petTypeRepository = petTypeRepository;
    }

    @Override
    public Set<PetType> findAll() {
        Set<PetType> petTypes = new HashSet<>();
        petTypeRepository.findAll().forEach(petTypes::add);
        return petTypes;
    }

    @Override
    public PetType findById(Long id) {
        return petTypeRepository.findById(id).orElse(null);
    }

    @Override
    public PetType save(PetType object) {
        return petTypeRepository.save(object);
    }

    @Override
    public void delete(PetType object) {
       petTypeRepository.delete(object);
    }

    @Override
    public void deleteByID(Long id) {
        //nasaan yung method, pangalan ng method, value na kailangan mo i supply
        petTypeRepository.deleteById(id);
    }
}
