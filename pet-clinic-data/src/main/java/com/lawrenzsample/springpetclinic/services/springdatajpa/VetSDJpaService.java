package com.lawrenzsample.springpetclinic.services.springdatajpa;

import com.lawrenzsample.springpetclinic.model.Vet;
import com.lawrenzsample.springpetclinic.repositories.VetRepository;
import com.lawrenzsample.springpetclinic.services.VetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class VetSDJpaService implements VetService {

    /*
    * 1. declare as service
    * 2. annotate profile - springdatajpa
    * 3. implements VetService (service class)
    * 4. declare repository class
    * 5. constructor
    **/

    private final VetRepository vetRepository;

    public VetSDJpaService(VetRepository vetRepository) {
        this.vetRepository = vetRepository;
    }

    @Override
    public Set<Vet> findAll() {
        Set<Vet> vets = new HashSet<>(); //always hashset para di null

        vetRepository.findAll().forEach(vets::add);

        return vets;
    }

    @Override
    public Vet findById(Long id) {
        return vetRepository.findById(id).orElse(null);
    }

    @Override
    public Vet save(Vet object) {
        return vetRepository.save(object);
    }

    @Override
    public void delete(Vet object) {
        vetRepository.delete(object);
    }

    @Override
    public void deleteByID(Long id) {
        vetRepository.deleteById(id);
    }
}
