package com.lawrenzsample.springpetclinic.services.springdatajpa;

import com.lawrenzsample.springpetclinic.model.Visit;
import com.lawrenzsample.springpetclinic.repositories.VisitRepository;
import com.lawrenzsample.springpetclinic.services.VisitService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class VisitSDJpaService implements VisitService {

    private final VisitRepository visitRepository;

    public VisitSDJpaService(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    @Override
    public Set<Visit> findAll() {
        //declare a set and initiliaze it with Hashset
        Set<Visit> visits = new HashSet<>();

        //call FindAll method
        visitRepository.findAll().forEach(visits :: add);
        return visits;
    }

    @Override
    public Visit findById(Long id) {
        return visitRepository.findById(id).orElse(null);
    }

    @Override
    public Visit save(Visit visit) {
        return visitRepository.save(visit);
    }

    @Override
    public void delete(Visit visit) {
        visitRepository.delete(visit);
    }

    @Override
    public void deleteByID(Long id) {
        visitRepository.deleteById(id);
    }
}
